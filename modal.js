/**
 * Cwiczenie 1 - Modal
 *
 * a) caly modal jest budowany w JS
 * b) modal czeka w HTML na wywolanie przez JS (tak jak spinner)
 *
 * Co potrzebujemy:
 * a) CSS: position, top/left, z-index, background, ...
 * b) HTML: container, w container - modal, w modalu: [X] do zamykania, tekst (lorem ipsum), input + button
 * c) JS: setTimeout, querySelector..., element.style.visibility.., addEventListner na ikonie do zamykania
 */
//modal container powinenm miec 100% width i height

// * Cwiczenie 2 - Modal - obsluga
//  * a) wyswietlanie / interakcja
//  * - klik na "X" powoduje zamkniecie
//  * - modal pojawia sie automatycznie po 10sek
//  * - obsluga zapisania sie do newslettera: wysylka formularza + komunikat o zapisaniu
//  *
//  * b) zapisywanie informacji w przegladarce
//  * - wykorzystaj localStorage do sprawdzania czy modal byl wyswietlony
//  */

// Obsluga wysylki formularza w modalu
// 1. Wybranie formularza z modala - modalContainer.querySelector
// 2. Dodajemy event na submit
// 3. obsluga - preventDefault() + podmiana zawartosci innerHtml w naszym modalu (u mnie to będzie klasa ".modal__content")

const modal = document.querySelector(".modalContainer");
const modalBox = document.querySelector(".modal");
const closeSpan = document.querySelector(".close");
const body = document.querySelector("body");
const modalInput = document.querySelector(".modalInput");

// const showModal = () => {
//   let item = localStorage.getItem("counter");
//   setItemFromStorage("counter", { item: 1 });
//   localStorage.setItem("counter", ++item);
//   console.log(!localStorage.getItem("newsletterModal"));
//   if (item >= 3) {
//     setItemFromStorage("newsletterModal", "@");
//     return;
//   }
//   checkModalInterval();
//   setTimeout(() => console.log(item), 5000);
//   modalBox.style.visibility = "visible";
//   modal.style.backgroundColor = "rgba(26, 26, 26, 0.3)";
//   clearTimeout(modalTimer);
// };
// let modalTimer = setTimeout(showModal, 3000);
const checkModalInterval = () => {
  if (
    !localStorage.getItem("newsletterModal") &&
    modalBox.style.visibility === "hidden"
  ) {
    modalTimer = setTimeout(showModal, 3000);
  }
};
const closeModalstyles = () => {
  modalBox.style.visibility = "hidden";
  modal.style.backgroundColor = "inherit";
  checkModalInterval();
};

const form = modal.querySelector("form");
body.addEventListener("keydown", (e) => {
  if (e.code == "Escape" && modalBox.style.visibility === "visible") {
    closeModalstyles();
  }
});
//ZAD DOM Klikanie poza modal i zamykanie go
// body.addEventListener("click", (e) => {
//   console.log("x" + e.x, "y" + e.y, e);
//   if (
//     (e.x < 580 && e.y > 155) ||
//     (e.x > 1080 && e.y < 400) ||
//     (e.x > 580 && e.y > 400) ||
//     (e.x > 580 && e.y < 155)
//   )
//     closeModalstyles();
// });

// closeSpan.addEventListener("click", closeModalstyles);

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const modalContent = modal.querySelector(".modal__content");
  if (modalInput.value !== "") {
    modalContent.innerText = "Dziękujemy za zapisanie się";
    if (window.localStorage) {
      localStorage.setItem("newsletterModal", modalInput.value);
    }
    setTimeout(() => (modalBox.style.visibility = "hidden"), 2000);
  } else {
    modalContent.innerText = "Nieprawidłowy format maila";
    modalInput.value = "";
  }
});

const getItemFromStorage = (key) => {
  window.localStorage
    ? localStorage.getItem(key)
    : console.error("LocalStorage not supported");
};
const setItemFromStorage = (key, value) => {
  window.localStorage
    ? localStorage.setItem(key, JSON.stringify(value))
    : console.error("LocalStorage not supported");
};

// Cwiczenie 4 - localstorage & modal
// Modal wyswietlamy tylko w sytuacji, kiedy localstorage od "newsletterModal" jest puste.
// W kazdej sytuacji, kiedy uzytkownik wejdzie w interakcje z modalem - ustawiamy "newsletterModal" na "true" X

/**
 * Cwiczenie 6
 * Utworz modal logowania/rejstracji
 *
 * Zalozenia:
 * - modal posiada dwie zakladki
 * - zakladka logowania zawiera dwa inputy (mail + pwd) oraz przycisk "loguj"
 * - zakladka "rejestracja" bedzie zawierala 2 stepy tzn:
 * -- step1 - dane osobowe (imie + nazwisko)
 * -- step2 - mail + pwd
 *
 * Do zakladki "rejestracja" potrzebny nam będzie stepper ktory pokaze aktualny krok rejestracji
 * - dopoki dane nie beda prawidlowe przyciski typu "nastepny"/"rejestruj" beda nieaktywne
 * - przycisk "wstecz" jest aktywny tylko na step2
 * - przycisk "rejestruj" spowoduje zamkniecie modala i wyswietlenie informacji o prawidlowej rejestracji
 */
const content1 = document.querySelector(".content2");
const switchModals = document.querySelector(".modalRegisterSwitch");
const stepButton = document.querySelector(".stepButton");
const stepCircles = document.querySelectorAll(".circle");
const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");
const labelFirst = document.querySelector("label[for='firstName']");
const labelSecond = document.querySelector("label[for='lastName']");
const registerButton = document.querySelector("#register");

const buttons = document.querySelectorAll(".button");
buttons.forEach((button) => {
  button.addEventListener("click", (e) => {
    if (e.target.id) {
      document.querySelectorAll(".button").forEach((div) => {
        div.classList.remove("active");
      });
      const activeDiv = document.getElementById(e.target.id);
      activeDiv.classList.add("active");
      document.querySelectorAll(".content2").forEach((div) => {
        if (div.dataset.id === e.target.id) {
          div.style.display = "block";
        } else {
          div.style.display = "none";
        }
      });
    }
  });
});

const isButtonDisabled = () => {
  if (firstName.value == "" && lastName.value == "") {
    registerButton.classList.add("disabled");
    stepButton.classList.add("disabled");
    // stepButton.disabled = true; //
  } else {
    registerButton.classList.remove("disabled");
    stepButton.classList.remove("disabled");
    stepButton.disabled = false;
  } ///////////////////////////////////////
};
const firstNameEV = firstName.addEventListener("input", () => {
  isButtonDisabled();
});
const buttonInnerText = () => {
  console.log(stepCircles[0].classList.contains("disabled"));

  if (stepCircles[0].classList.contains("disabled")) {
    stepButton.innerHTML = "Cofnij";
  } else {
    stepButton.innerHTML = "Dalej";
  }
};
const labelForFirstNameInnerHTML = labelFirst.innerHTML;
const labelForLastNameInnerHTML = labelSecond.innerHTML;
const labelSwitch = () => {
  if (firstName.value && lastName.value && stepButton.innerText === "Dalej") {
    labelFirst.innerHTML = `Podaj email <input required id="email" "type="email" placeholder="email" class="inputs">`;
    labelSecond.innerHTML = `Utwórz hasło <input required id="password" type="password" placeholder="hasło" class="inputs> class="inputs">`;
  } else {
    labelFirst.innerHTML = labelForFirstNameInnerHTML;
    labelSecond.innerHTML = labelForLastNameInnerHTML;
  }
};
isButtonDisabled();
buttonInnerText();
stepButton.addEventListener("click", () => {
  console.log("click");
  if (firstName.value && lastName.value && stepButton.innerText === "Dalej") {
    labelSwitch();
    stepCircles[1].classList.remove("disabled");
    stepCircles[0].classList.add("disabled");
    buttonInnerText();
  } else {
    labelSwitch();
    stepCircles[0].classList.remove("disabled");
    stepCircles[1].classList.add("disabled");
    buttonInnerText();
  }
});

const checkRegisterForm = () => {
  const name = firstName.value;
  const lastN = lastName.value;

  if (
    name.length > 0 &&
    lastN.length > 0 &&
    stepButton.innerText === "Cofnij"
  ) {
    registerButton.innerText = "Zarejestruj ";
    const email = document.querySelector("#email").value;
    const password = document.querySelector("#password").value;
    if (email.length > 0 && password.length > 0) {
      switchModals.innerHTML = "Uzytkownik zajerestrowany";
      setTimeout(() => {
        switchModals.style.display = "none";
      }, 3000);
    }
  }
};
registerButton.addEventListener("click", checkRegisterForm);
